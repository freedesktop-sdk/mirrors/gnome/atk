# Indonesia translation of atk HEAD.
# Copyright (C) 2005 THE atk's COPYRIGHT HOLDER
# This file is distributed under the same license as the atk package.
#
# Mohammad DAMT <mdamt@bisnisweb.com>, 2005.
# Ahmad Riza H Nst  <rizahnst@gnome.org>, 2006.
# Andika Triwidada <andika@gmail.com>, 2010, 2011, 2014.
# Dirgita <dirgitadevina@yahoo.co.id>, 2010.
# Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: atk master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/atk/issues\n"
"POT-Creation-Date: 2020-06-06 13:32+0000\n"
"PO-Revision-Date: 2021-12-22 09:49+0700\n"
"Last-Translator: Kukuh Syafaat <kukuhsyafaat@gnome.org>\n"
"Language-Team: GNOME Indonesian Translation Team <gnome@i15n.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: atk/atkhyperlink.c:126
msgid "Selected Link"
msgstr "Taut Terpilih"

#: atk/atkhyperlink.c:127
msgid "Specifies whether the AtkHyperlink object is selected"
msgstr "Menentukan apakah objek AtkHyperlink dipilih atau tidak"

#: atk/atkhyperlink.c:133
msgid "Number of Anchors"
msgstr "Jumlah Jangkar/Anchor"

#: atk/atkhyperlink.c:134
msgid "The number of anchors associated with the AtkHyperlink object"
msgstr "Jumlah jangkar/anchor yang dihubungkan dengan objek AtkHyperlink"

#: atk/atkhyperlink.c:142
msgid "End index"
msgstr "Indeks akhir"

#: atk/atkhyperlink.c:143
msgid "The end index of the AtkHyperlink object"
msgstr "Akhir indeks objek AtkHyperlink"

#: atk/atkhyperlink.c:151
msgid "Start index"
msgstr "Indeks awal"

#: atk/atkhyperlink.c:152
msgid "The start index of the AtkHyperlink object"
msgstr "Indeks awal objek AtkHyperlink"

#: atk/atkobject.c:98
msgid "invalid"
msgstr "tidak sah"

#: atk/atkobject.c:99
msgid "accelerator label"
msgstr "label akselerator"

#: atk/atkobject.c:100
msgid "alert"
msgstr "waspada"

#: atk/atkobject.c:101
msgid "animation"
msgstr "animasi"

#: atk/atkobject.c:102
msgid "arrow"
msgstr "panah"

#: atk/atkobject.c:103
msgid "calendar"
msgstr "kalender"

#: atk/atkobject.c:104
msgid "canvas"
msgstr "kanvas"

#: atk/atkobject.c:105
msgid "check box"
msgstr "kotak cek"

#: atk/atkobject.c:106
msgid "check menu item"
msgstr "item menu cek"

#: atk/atkobject.c:107
msgid "color chooser"
msgstr "pemilih warna"

#: atk/atkobject.c:108
msgid "column header"
msgstr "tajuk kolom"

#: atk/atkobject.c:109
msgid "combo box"
msgstr "kotak kombo"

#: atk/atkobject.c:110
msgid "dateeditor"
msgstr "penyunting tanggal"

#: atk/atkobject.c:111
msgid "desktop icon"
msgstr "ikon desktop"

#: atk/atkobject.c:112
msgid "desktop frame"
msgstr "bingkai desktop"

#: atk/atkobject.c:113
msgid "dial"
msgstr "tombol pemutar"

#: atk/atkobject.c:114
msgid "dialog"
msgstr "dialog"

#: atk/atkobject.c:115
msgid "directory pane"
msgstr "panel direktori"

#: atk/atkobject.c:116
msgid "drawing area"
msgstr "area gambar"

#: atk/atkobject.c:117
msgid "file chooser"
msgstr "pemilih berkas"

#: atk/atkobject.c:118
msgid "filler"
msgstr "isian"

#. I know it looks wrong but that is what Java returns
#: atk/atkobject.c:120
msgid "fontchooser"
msgstr "pemilih fonta"

#: atk/atkobject.c:121
msgid "frame"
msgstr "bingkai"

#: atk/atkobject.c:122
msgid "glass pane"
msgstr "panel kaca"

#: atk/atkobject.c:123
msgid "html container"
msgstr "kontainer html"

#: atk/atkobject.c:124
msgid "icon"
msgstr "ikon"

#: atk/atkobject.c:125
msgid "image"
msgstr "gambar"

#: atk/atkobject.c:126
msgid "internal frame"
msgstr "bingkai internal"

#: atk/atkobject.c:127
msgid "label"
msgstr "label"

#: atk/atkobject.c:128
msgid "layered pane"
msgstr "panel  berlapis"

#: atk/atkobject.c:129
msgid "list"
msgstr "daftar"

#: atk/atkobject.c:130
msgid "list item"
msgstr "isi daftar"

#: atk/atkobject.c:131
msgid "menu"
msgstr "menu"

#: atk/atkobject.c:132
msgid "menu bar"
msgstr "baris menu"

#: atk/atkobject.c:133
msgid "menu item"
msgstr "objek menu"

#: atk/atkobject.c:134
msgid "option pane"
msgstr "panel opsi"

#: atk/atkobject.c:135
msgid "page tab"
msgstr "halaman tab"

#: atk/atkobject.c:136
msgid "page tab list"
msgstr "daftar halaman tab"

#: atk/atkobject.c:137
msgid "panel"
msgstr "panel"

#: atk/atkobject.c:138
msgid "password text"
msgstr "teks sandi"

#: atk/atkobject.c:139
msgid "popup menu"
msgstr "menu tongol"

#: atk/atkobject.c:140
msgid "progress bar"
msgstr "indikator proses"

#: atk/atkobject.c:141
msgid "push button"
msgstr "tombol tekan"

#: atk/atkobject.c:142
msgid "radio button"
msgstr "tombol radio"

#: atk/atkobject.c:143
msgid "radio menu item"
msgstr "objek menu radio"

#: atk/atkobject.c:144
msgid "root pane"
msgstr "panel dasar"

#: atk/atkobject.c:145
msgid "row header"
msgstr "tajuk baris"

#: atk/atkobject.c:146
msgid "scroll bar"
msgstr "batang penggulung"

#: atk/atkobject.c:147
msgid "scroll pane"
msgstr "panel penggulung"

#: atk/atkobject.c:148
msgid "separator"
msgstr "pemisah"

#: atk/atkobject.c:149
msgid "slider"
msgstr "slider"

#: atk/atkobject.c:150
msgid "split pane"
msgstr "panel pemisah"

#: atk/atkobject.c:151
msgid "spin button"
msgstr "tombol putar"

#: atk/atkobject.c:152
msgid "statusbar"
msgstr "baris status"

#: atk/atkobject.c:153
msgid "table"
msgstr "tabel"

#: atk/atkobject.c:154
msgid "table cell"
msgstr "sel tabel"

#: atk/atkobject.c:155
msgid "table column header"
msgstr "tajuk kolom pada tabel"

#: atk/atkobject.c:156
msgid "table row header"
msgstr "tajuk baris pada tabel"

#: atk/atkobject.c:157
msgid "tear off menu item"
msgstr "anggota menu yang dapat dilepas"

#: atk/atkobject.c:158
msgid "terminal"
msgstr "terminal"

#: atk/atkobject.c:159
msgid "text"
msgstr "teks"

#: atk/atkobject.c:160
msgid "toggle button"
msgstr "tombol togel"

#: atk/atkobject.c:161
msgid "tool bar"
msgstr "bilah alat"

#: atk/atkobject.c:162
msgid "tool tip"
msgstr "teks bantuan"

#: atk/atkobject.c:163
msgid "tree"
msgstr "pohon"

#: atk/atkobject.c:164
msgid "tree table"
msgstr "tabel pohon"

#: atk/atkobject.c:165
msgid "unknown"
msgstr "tidak diketahui"

#: atk/atkobject.c:166
msgid "viewport"
msgstr "viewport"

#: atk/atkobject.c:167
msgid "window"
msgstr "jendela"

#: atk/atkobject.c:168
msgid "header"
msgstr "tajuk"

#: atk/atkobject.c:169
msgid "footer"
msgstr "kaki"

#: atk/atkobject.c:170
msgid "paragraph"
msgstr "paragraf"

#: atk/atkobject.c:171
msgid "ruler"
msgstr "penggaris"

#: atk/atkobject.c:172
msgid "application"
msgstr "aplikasi"

#: atk/atkobject.c:173
msgid "autocomplete"
msgstr "lengkap secara otomatis"

#: atk/atkobject.c:174
msgid "edit bar"
msgstr "bilah sunting"

#: atk/atkobject.c:175
msgid "embedded component"
msgstr "komponen tercangkok"

#: atk/atkobject.c:176
msgid "entry"
msgstr "entri"

#: atk/atkobject.c:177
msgid "chart"
msgstr "bagan"

#: atk/atkobject.c:178
msgid "caption"
msgstr "kapsi"

#: atk/atkobject.c:179
msgid "document frame"
msgstr "bingkai dokumen"

#: atk/atkobject.c:180
msgid "heading"
msgstr "tajuk"

#: atk/atkobject.c:181
msgid "page"
msgstr "halaman"

#: atk/atkobject.c:182
msgid "section"
msgstr "bagian"

#: atk/atkobject.c:183
msgid "redundant object"
msgstr "objek redundan"

#: atk/atkobject.c:184
msgid "form"
msgstr "formulir"

#: atk/atkobject.c:185
msgid "link"
msgstr "taut"

#: atk/atkobject.c:186
msgid "input method window"
msgstr "jendela metoda masukan"

#: atk/atkobject.c:187
msgid "table row"
msgstr "baris tabel"

#: atk/atkobject.c:188
msgid "tree item"
msgstr "butir tabel pohon"

#: atk/atkobject.c:189
msgid "document spreadsheet"
msgstr "spreadsheet dokumen"

#: atk/atkobject.c:190
msgid "document presentation"
msgstr "presentasi dokumen"

#: atk/atkobject.c:191
msgid "document text"
msgstr "teks dokumen"

#: atk/atkobject.c:192
msgid "document web"
msgstr "web dokumen"

#: atk/atkobject.c:193
msgid "document email"
msgstr "surel dokumen"

#: atk/atkobject.c:194
msgid "comment"
msgstr "komentar"

#: atk/atkobject.c:195
msgid "list box"
msgstr "kotak daftar"

#: atk/atkobject.c:196
msgid "grouping"
msgstr "pengelompokan"

#: atk/atkobject.c:197
msgid "image map"
msgstr "peta gambar"

#: atk/atkobject.c:198
msgid "notification"
msgstr "pemberitahuan"

#: atk/atkobject.c:199
msgid "info bar"
msgstr "bilah info"

#: atk/atkobject.c:200
msgid "level bar"
msgstr "bilah aras"

#: atk/atkobject.c:201
msgid "title bar"
msgstr "bilah judul"

#: atk/atkobject.c:202
msgid "block quote"
msgstr "kutip blok"

#: atk/atkobject.c:203
msgid "audio"
msgstr "audio"

#: atk/atkobject.c:204
msgid "video"
msgstr "video"

#: atk/atkobject.c:205
msgid "definition"
msgstr "definisi"

#: atk/atkobject.c:206
msgid "article"
msgstr "artikel"

#: atk/atkobject.c:207
msgid "landmark"
msgstr "landmark"

#: atk/atkobject.c:208
msgid "log"
msgstr "log"

#: atk/atkobject.c:209
msgid "marquee"
msgstr "marquee"

#: atk/atkobject.c:210
msgid "math"
msgstr "math"

#: atk/atkobject.c:211
msgid "rating"
msgstr "rating"

#: atk/atkobject.c:212
msgid "timer"
msgstr "timer"

#: atk/atkobject.c:213
msgid "description list"
msgstr "daftar deskripsi"

#: atk/atkobject.c:214
msgid "description term"
msgstr "istilah deskripsi"

#: atk/atkobject.c:215
msgid "description value"
msgstr "nilai deskripsi"

#: atk/atkobject.c:391
msgid "Accessible Name"
msgstr "Nama Akses"

#: atk/atkobject.c:392
msgid "Object instance’s name formatted for assistive technology access"
msgstr "Nama turunan objek yang digunakan untuk teknologi kemudahan akses"

#: atk/atkobject.c:398
msgid "Accessible Description"
msgstr "Keterangan Akses"

#: atk/atkobject.c:399
msgid "Description of an object, formatted for assistive technology access"
msgstr "Keterangan objek yang digunakan dalam teknologi kemudahan akses"

#: atk/atkobject.c:405
msgid "Accessible Parent"
msgstr "Induk Akses"

#: atk/atkobject.c:406
msgid "Parent of the current accessible as returned by atk_object_get_parent()"
msgstr ""
"Induk akses saat ini sebagaimana dikembalikan oleh atk_object_get_parent()"

#: atk/atkobject.c:422
msgid "Accessible Value"
msgstr "Nilai Akses"

#: atk/atkobject.c:423
msgid "Is used to notify that the value has changed"
msgstr "Digunakan untuk memberitahukan bahwa nilai telah berubah"

#: atk/atkobject.c:431
msgid "Accessible Role"
msgstr "Peran Akses"

#: atk/atkobject.c:432
msgid "The accessible role of this object"
msgstr "Peran akses objek ini"

#: atk/atkobject.c:439
msgid "Accessible Layer"
msgstr "Tapis Akses"

#: atk/atkobject.c:440
msgid "The accessible layer of this object"
msgstr "Tapis akses objek ini"

#: atk/atkobject.c:448
msgid "Accessible MDI Value"
msgstr "Nilai MDI Akses"

#: atk/atkobject.c:449
msgid "The accessible MDI value of this object"
msgstr "Nilai MDI akses objek ini"

#: atk/atkobject.c:465
msgid "Accessible Table Caption"
msgstr "Judul Tabel Akses"

#: atk/atkobject.c:466
msgid ""
"Is used to notify that the table caption has changed; this property should "
"not be used. accessible-table-caption-object should be used instead"
msgstr ""
"Digunakan untuk memberi peringatan bahwa judul tabel telah berubah,nilai ini "
"sebaiknya tidak digunakan, silakan gunakan accessible-table-caption-object "
"saja"

#: atk/atkobject.c:480
msgid "Accessible Table Column Header"
msgstr "Kepala Kolom Tabel Akses"

#: atk/atkobject.c:481
msgid "Is used to notify that the table column header has changed"
msgstr "Digunakan untuk memberitahu bahwa kepala kolom telah berubah"

#: atk/atkobject.c:496
msgid "Accessible Table Column Description"
msgstr "Keterangan Kolom Tabel Akses"

#: atk/atkobject.c:497
msgid "Is used to notify that the table column description has changed"
msgstr "Digunakan untuk memberitahu bahwa keterangan kolom tabel telah berubah"

#: atk/atkobject.c:512
msgid "Accessible Table Row Header"
msgstr "Kepala Baris Tabel Akses"

#: atk/atkobject.c:513
msgid "Is used to notify that the table row header has changed"
msgstr "Digunakan untuk memberitahu bawha kepala baris tabel telah berubah"

#: atk/atkobject.c:527
msgid "Accessible Table Row Description"
msgstr "Keterangan Baris Tabel Akses"

#: atk/atkobject.c:528
msgid "Is used to notify that the table row description has changed"
msgstr "Digunakan untuk memberitahu bahwa keterangan baris tabel telah berubah"

#: atk/atkobject.c:534
msgid "Accessible Table Summary"
msgstr "Ringkasan Tabel Akses"

#: atk/atkobject.c:535
msgid "Is used to notify that the table summary has changed"
msgstr "Digunakan untuk memberitahu bahwa keterangan tabel telah berubah"

#: atk/atkobject.c:541
msgid "Accessible Table Caption Object"
msgstr "Objek Judul Tabel Akses"

#: atk/atkobject.c:542
msgid "Is used to notify that the table caption has changed"
msgstr "Digunakan untuk memberitahu bahwa judul tabel telah berubah"

#: atk/atkobject.c:548
msgid "Number of Accessible Hypertext Links"
msgstr "Jumlah Akses Taut Hiperteks"

#: atk/atkobject.c:549
msgid "The number of links which the current AtkHypertext has"
msgstr "Jumlah taut yang dimiliki AtkHypertext saat ini"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:194
msgid "very weak"
msgstr "sangat lemah"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:201
msgid "weak"
msgstr "lemah"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:208
msgid "acceptable"
msgstr "dapat diterima"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:215
msgid "strong"
msgstr "kuat"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:222
msgid "very strong"
msgstr "sangat kuat"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:230
msgid "very low"
msgstr "sangat rendah"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:238
msgid "medium"
msgstr "sedang"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:246
msgid "high"
msgstr "tinggi"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:254
msgid "very high"
msgstr "sangat tinggi"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:262
msgid "very bad"
msgstr "sangat buruk"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:270
msgid "bad"
msgstr "buruk"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:278
msgid "good"
msgstr "baik"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:286
msgid "very good"
msgstr "sangat baik"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:294
msgid "best"
msgstr "terbaik"
