# translation of atk.gnome-2-14.ne.po to Nepali
# translation of atk.gnome-2-10.ne.po to Nepali
# translation of atk.gnome-2-10.po to Nepali
# translation of atk.HEAD.po to Nepali
# translation of atk.HEAD.po to
# translation of atk.HEAD.ne.po to
# This file is distributed under the same license as the atk package.
# Pawan Chitrakar <pawan@nplinux.org>.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Pawan Chitrakar,,, <pawan@mpp.org.np>, 2005.
# Ganesh Ghimire <gghimire@gmail.com>, 2005.
# Jyotshna Shrestha <jyotshna@mpp.org.np>, 2005.
# Jaydeep Bhusal <zaydeep@hotmail.com>, 2005.
# Kapil Timilsina <lipak21@gmail.com>, 2005.
# Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: Gnome Nepali Translation Project\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?"
"product=atk&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-07-25 13:57+0000\n"
"PO-Revision-Date: 2017-10-18 14:55+0545\n"
"Last-Translator: Pawan Chitrakar <chautari@gmail.com>\n"
"Language-Team: Nepali Translation Team <chautari@gmail.com>\n"
"Language: ne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.4\n"
"Plural-Forms: nplurals=2;plural=(n!=1);\n"

#: atk/atkhyperlink.c:126
msgid "Selected Link"
msgstr "चयन गरिएको लिङ्क"

#: atk/atkhyperlink.c:127
msgid "Specifies whether the AtkHyperlink object is selected"
msgstr "AtkHyperlink वस्तु चयन गरिएको छ कि छैन निश्चित गर्नुहोस्"

#: atk/atkhyperlink.c:133
msgid "Number of Anchors"
msgstr "एङ्करहरूको सङ्ख्या"

#: atk/atkhyperlink.c:134
msgid "The number of anchors associated with the AtkHyperlink object"
msgstr "AtkHyperlink वस्तुसँग सम्बद्ध एङ्करहरूको सङ्ख्या"

#: atk/atkhyperlink.c:142
msgid "End index"
msgstr "अन्तिम अनुक्रमणिका"

#: atk/atkhyperlink.c:143
msgid "The end index of the AtkHyperlink object"
msgstr "AtkHyperlink वस्तुको अन्तिम अनुक्रमणिका"

#: atk/atkhyperlink.c:151
msgid "Start index"
msgstr "सुरु अनुक्रमणिका"

#: atk/atkhyperlink.c:152
msgid "The start index of the AtkHyperlink object"
msgstr "AtkHyperlink वस्तुको प्रथम अनुक्रमणिका"

#: atk/atkobject.c:97
msgid "invalid"
msgstr "अवैध"

#: atk/atkobject.c:98
msgid "accelerator label"
msgstr "गतिवर्धक लेबुल"

#: atk/atkobject.c:99
msgid "alert"
msgstr "सावधान"

#: atk/atkobject.c:100
msgid "animation"
msgstr "एनिमेसन"

#: atk/atkobject.c:101
msgid "arrow"
msgstr "बाँण"

#: atk/atkobject.c:102
msgid "calendar"
msgstr "पात्रो"

#: atk/atkobject.c:103
msgid "canvas"
msgstr "क्यानभास"

#: atk/atkobject.c:104
msgid "check box"
msgstr "जाँच बाकस"

#: atk/atkobject.c:105
msgid "check menu item"
msgstr "मेनु वस्तु जाँच्नुहोस्"

#: atk/atkobject.c:106
msgid "color chooser"
msgstr "रङ चयनकर्ता"

#: atk/atkobject.c:107
msgid "column header"
msgstr "स्तम्भ हेडर"

#: atk/atkobject.c:108
msgid "combo box"
msgstr "कम्बो बाकस"

#: atk/atkobject.c:109
msgid "dateeditor"
msgstr "मिति सम्पादक"

#: atk/atkobject.c:110
msgid "desktop icon"
msgstr "डेस्कटप प्रतिमा"

#: atk/atkobject.c:111
msgid "desktop frame"
msgstr "डेस्कटप फ्रेम"

#: atk/atkobject.c:112
msgid "dial"
msgstr "डायल"

#: atk/atkobject.c:113
msgid "dialog"
msgstr "संवाद"

#: atk/atkobject.c:114
msgid "directory pane"
msgstr "डाइरेक्टरी फलक"

#: atk/atkobject.c:115
msgid "drawing area"
msgstr "रेखाचित्र क्षेत्र"

#: atk/atkobject.c:116
msgid "file chooser"
msgstr "फाइल चयनकर्ता"

#: atk/atkobject.c:117
msgid "filler"
msgstr "फिलर"

#  I know it looks wrong but that is what Java returns
#. I know it looks wrong but that is what Java returns
#: atk/atkobject.c:119
msgid "fontchooser"
msgstr "फन्ट चयनकर्ता"

#: atk/atkobject.c:120
msgid "frame"
msgstr "फ्रेम"

#: atk/atkobject.c:121
msgid "glass pane"
msgstr "ग्लास फलक"

#: atk/atkobject.c:122
msgid "html container"
msgstr "html भाँडो"

#: atk/atkobject.c:123
msgid "icon"
msgstr "प्रतिमा"

#: atk/atkobject.c:124
msgid "image"
msgstr "छवि"

#: atk/atkobject.c:125
msgid "internal frame"
msgstr "आन्तरिक फ्रेम"

#: atk/atkobject.c:126
msgid "label"
msgstr "लेबुल"

#: atk/atkobject.c:127
msgid "layered pane"
msgstr "तहगत फलक"

#: atk/atkobject.c:128
msgid "list"
msgstr "सूची"

#: atk/atkobject.c:129
msgid "list item"
msgstr "सूची वस्तु"

#: atk/atkobject.c:130
msgid "menu"
msgstr "मेनु"

#: atk/atkobject.c:131
msgid "menu bar"
msgstr "मेनु पट्टि"

#: atk/atkobject.c:132
msgid "menu item"
msgstr "मेनु वस्तु"

#: atk/atkobject.c:133
msgid "option pane"
msgstr "विकल्प फलक"

#: atk/atkobject.c:134
msgid "page tab"
msgstr "पृष्ठ ट्याब"

#: atk/atkobject.c:135
msgid "page tab list"
msgstr "पृष्ठ ट्याब सूची"

#: atk/atkobject.c:136
msgid "panel"
msgstr "प्यानल"

#: atk/atkobject.c:137
msgid "password text"
msgstr "पासवर्ड पाठ"

#: atk/atkobject.c:138
msgid "popup menu"
msgstr "पपअप मेनु"

#: atk/atkobject.c:139
msgid "progress bar"
msgstr "प्रगति पट्टि"

#: atk/atkobject.c:140
msgid "push button"
msgstr "थिच्ने बटन"

#: atk/atkobject.c:141
msgid "radio button"
msgstr "रेडियो बटन"

#: atk/atkobject.c:142
msgid "radio menu item"
msgstr "रेडियो मेनु वस्तु"

#: atk/atkobject.c:143
msgid "root pane"
msgstr "मूल फलक"

#: atk/atkobject.c:144
msgid "row header"
msgstr "पङ्क्ति हेडर"

#: atk/atkobject.c:145
msgid "scroll bar"
msgstr "स्क्रोलपट्टी"

#: atk/atkobject.c:146
msgid "scroll pane"
msgstr "स्क्रोल फलक"

#: atk/atkobject.c:147
msgid "separator"
msgstr "विभाजक"

#: atk/atkobject.c:148
msgid "slider"
msgstr "स्लाइडर"

#: atk/atkobject.c:149
msgid "split pane"
msgstr "छुट्टिएको फलक"

#: atk/atkobject.c:150
msgid "spin button"
msgstr "स्पिन बटन"

#: atk/atkobject.c:151
msgid "statusbar"
msgstr "वस्तुस्थितिपट्टी"

#: atk/atkobject.c:152
msgid "table"
msgstr "तालिका"

#: atk/atkobject.c:153
msgid "table cell"
msgstr "तालिका कक्ष"

#: atk/atkobject.c:154
msgid "table column header"
msgstr "तालिका स्तम्भ हेडर"

#: atk/atkobject.c:155
msgid "table row header"
msgstr "तालिका पङ्क्ति हेडर"

#: atk/atkobject.c:156
msgid "tear off menu item"
msgstr "मेनु वस्तु छुट्याउनुहोस्"

#: atk/atkobject.c:157
msgid "terminal"
msgstr "टर्मिनल"

#: atk/atkobject.c:158
msgid "text"
msgstr "पाठ"

#: atk/atkobject.c:159
msgid "toggle button"
msgstr "टगल बटन"

#: atk/atkobject.c:160
msgid "tool bar"
msgstr "उपकरणपट्टी"

#: atk/atkobject.c:161
msgid "tool tip"
msgstr "टुल टिप"

#: atk/atkobject.c:162
msgid "tree"
msgstr "ट्रि"

#: atk/atkobject.c:163
msgid "tree table"
msgstr "ट्रि तालिका"

#: atk/atkobject.c:164
msgid "unknown"
msgstr "अज्ञात"

#: atk/atkobject.c:165
msgid "viewport"
msgstr "दृश्य पोर्ट"

#: atk/atkobject.c:166
msgid "window"
msgstr "सञ्झ्याल"

#: atk/atkobject.c:167
msgid "header"
msgstr "हेडर"

#: atk/atkobject.c:168
msgid "footer"
msgstr "फुटर"

#: atk/atkobject.c:169
msgid "paragraph"
msgstr "अनुच्छेद"

#: atk/atkobject.c:170
msgid "ruler"
msgstr "रुलर"

#: atk/atkobject.c:171
msgid "application"
msgstr "अनुप्रयोग"

#: atk/atkobject.c:172
msgid "autocomplete"
msgstr "स्वत:समाप्ती"

#: atk/atkobject.c:173
msgid "edit bar"
msgstr "सम्पादन पट्टी"

#: atk/atkobject.c:174
msgid "embedded component"
msgstr "सन्निबेश गरिएको अवयव"

#: atk/atkobject.c:175
msgid "entry"
msgstr "प्रविष्टि"

#: atk/atkobject.c:176
msgid "chart"
msgstr "चित्रपट"

#: atk/atkobject.c:177
msgid "caption"
msgstr "क्याप्सन"

#: atk/atkobject.c:178
msgid "document frame"
msgstr "कागजात फ्रेम"

#: atk/atkobject.c:179
msgid "heading"
msgstr "हेडिङ"

#: atk/atkobject.c:180
msgid "page"
msgstr "पृष्ठ"

#: atk/atkobject.c:181
msgid "section"
msgstr "सेक्सन"

#: atk/atkobject.c:182
msgid "redundant object"
msgstr "अतिरिक्त वस्तु"

#: atk/atkobject.c:183
msgid "form"
msgstr "फारम"

#: atk/atkobject.c:184
msgid "link"
msgstr "लिङ्क"

#: atk/atkobject.c:185
msgid "input method window"
msgstr "आयात विधि सञ्झ्याल"

#: atk/atkobject.c:186
msgid "table row"
msgstr "तालिका पङ्क्ति"

#: atk/atkobject.c:187
msgid "tree item"
msgstr "ट्रि सामाग्री"

#: atk/atkobject.c:188
msgid "document spreadsheet"
msgstr "सप्रेडशिट कागजात"

#: atk/atkobject.c:189
msgid "document presentation"
msgstr "प्रस्तुतिकरण कागजात"

#: atk/atkobject.c:190
msgid "document text"
msgstr "पाठ कागजात"

#: atk/atkobject.c:191
msgid "document web"
msgstr "कागजात वेब"

#: atk/atkobject.c:192
msgid "document email"
msgstr "इमेल कागजात"

#: atk/atkobject.c:193
msgid "comment"
msgstr "टिप्पणी"

#: atk/atkobject.c:194
msgid "list box"
msgstr "सुची बाकस"

#: atk/atkobject.c:195
msgid "grouping"
msgstr "समूहबद्ध"

#: atk/atkobject.c:196
msgid "image map"
msgstr "छवि मानचित्र"

#: atk/atkobject.c:197
msgid "notification"
msgstr "सूचना"

#: atk/atkobject.c:198
msgid "info bar"
msgstr "सूचना पट्टी"

#: atk/atkobject.c:199
msgid "level bar"
msgstr "निसानी पट्टी"

#: atk/atkobject.c:200
msgid "title bar"
msgstr "शीर्षक पट्टी"

#: atk/atkobject.c:201
msgid "block quote"
msgstr "बाक्लो उद्धरण"

#: atk/atkobject.c:202
msgid "audio"
msgstr "ध्वनी"

#: atk/atkobject.c:203
msgid "video"
msgstr "भिडियो"

#: atk/atkobject.c:204
msgid "definition"
msgstr "परिभाषा"

#: atk/atkobject.c:205
msgid "article"
msgstr "लेख"

#: atk/atkobject.c:206
msgid "landmark"
msgstr "भूमिनिसान"

#: atk/atkobject.c:207
msgid "log"
msgstr "दैनिकि"

#: atk/atkobject.c:208
msgid "marquee"
msgstr "मार्की"

#: atk/atkobject.c:209
msgid "math"
msgstr "गणित"

#: atk/atkobject.c:210
msgid "rating"
msgstr "दर निर्धारण"

#: atk/atkobject.c:211
msgid "timer"
msgstr "समयक"

#: atk/atkobject.c:212
msgid "description list"
msgstr "वर्णन सूची"

#: atk/atkobject.c:213
msgid "description term"
msgstr "वर्णन शब्द"

#: atk/atkobject.c:214
msgid "description value"
msgstr "वर्णन मान"

#: atk/atkobject.c:372
msgid "Accessible Name"
msgstr "पहुँचयोग्य नाम"

#: atk/atkobject.c:373
msgid "Object instance’s name formatted for assistive technology access"
msgstr "सहयोगी प्रविधि पहुंचका लागि वस्तु दृष्टान्तको नाम ढाँचा पारियो"

#: atk/atkobject.c:379
msgid "Accessible Description"
msgstr "पहुँचयोग्य वर्णन"

#: atk/atkobject.c:380
msgid "Description of an object, formatted for assistive technology access"
msgstr "एउटा वस्तुको वर्णन, सहयोगी प्रविधि पहुँचका लागि ढाँचा पारियो"

#: atk/atkobject.c:386
msgid "Accessible Parent"
msgstr "पहुँचयोग्य प्रमूल"

#: atk/atkobject.c:387
msgid "Parent of the current accessible as returned by atk_object_get_parent()"
msgstr "atk_object_get_parent() द्वारा फर्काइएको वर्तमान पहुँचको अभिभावक"

#: atk/atkobject.c:403
msgid "Accessible Value"
msgstr "पहुँचयोग्य मान"

#: atk/atkobject.c:404
msgid "Is used to notify that the value has changed"
msgstr "मान परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"

#: atk/atkobject.c:412
msgid "Accessible Role"
msgstr "पहुँचयोग्य भूमिका"

#: atk/atkobject.c:413
msgid "The accessible role of this object"
msgstr "यस वस्तुको पहुँच योग्य भूमिका"

#: atk/atkobject.c:421
msgid "Accessible Layer"
msgstr "पहुँच योग्य तह"

#: atk/atkobject.c:422
msgid "The accessible layer of this object"
msgstr "यस वस्तुको पहुँच योग्य तह"

#: atk/atkobject.c:430
msgid "Accessible MDI Value"
msgstr "पहुँच योग्य MDI मान"

#: atk/atkobject.c:431
msgid "The accessible MDI value of this object"
msgstr "यस वस्तुको पहुँच योग्य MDI मान"

#: atk/atkobject.c:447
msgid "Accessible Table Caption"
msgstr "पहुँच योग्य तालिका क्याप्सन"

#: atk/atkobject.c:448
msgid ""
"Is used to notify that the table caption has changed; this property should not be "
"used. accessible-table-caption-object should be used instead"
msgstr ""
"तालिका क्याप्सन परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ; यो गुण प्रयोग गर्न सकिँदैन। यसको "
"सट्टामा पहुँच योग्य-तालिका-क्याप्सन-वस्तु प्रयोग गर्नुहोस्"

#: atk/atkobject.c:462
msgid "Accessible Table Column Header"
msgstr "पहुँच योग्य तालिका स्तम्भ हेडर"

#: atk/atkobject.c:463
msgid "Is used to notify that the table column header has changed"
msgstr "तालिका स्तम्भ हेडर परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"

#: atk/atkobject.c:478
msgid "Accessible Table Column Description"
msgstr "पहुँच योग्य तालिका स्तम्भ वर्णन"

#: atk/atkobject.c:479
msgid "Is used to notify that the table column description has changed"
msgstr "तालिका स्तम्भ वर्णन परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"

#: atk/atkobject.c:494
msgid "Accessible Table Row Header"
msgstr "पहुँच योग्य तालिका पङ्क्ति हेडर"

#: atk/atkobject.c:495
msgid "Is used to notify that the table row header has changed"
msgstr "तालिका पङ्क्ति हेडर परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"

#: atk/atkobject.c:509
msgid "Accessible Table Row Description"
msgstr "पहुँच योग्य तालिका पङ्क्ति वर्णन"

#: atk/atkobject.c:510
msgid "Is used to notify that the table row description has changed"
msgstr "तालिका पङ्क्ति वर्णन परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"

#: atk/atkobject.c:516
msgid "Accessible Table Summary"
msgstr "पहुँच योग्य तालिका सारांश"

#: atk/atkobject.c:517
msgid "Is used to notify that the table summary has changed"
msgstr "तालिका सारांश परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"

#: atk/atkobject.c:523
msgid "Accessible Table Caption Object"
msgstr "पहुँच योग्य तालिका क्याप्सन वस्तु"

#: atk/atkobject.c:524
msgid "Is used to notify that the table caption has changed"
msgstr "तालिका क्याप्सन परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"

#: atk/atkobject.c:530
msgid "Number of Accessible Hypertext Links"
msgstr "पहुँच योग्य हाइपरटेक्स्ट लिङ्कहरूको सङ्ख्या"

#: atk/atkobject.c:531
msgid "The number of links which the current AtkHypertext has"
msgstr "हालको AtkHypertext संगरहेको लिङ्कहरूको सङ्ख्या"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:194
msgid "very weak"
msgstr "सार्‍है कमजोर"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:201
msgid "weak"
msgstr "कमजोर"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:208
msgid "acceptable"
msgstr "स्वीकार्य"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:215
msgid "strong"
msgstr "बलियो"

#. Translators: This string describes a range within value-related
#. * widgets such as a password-strength meter. Note that what such a
#. * widget presents is controlled by application developers. Thus
#. * assistive technologies such as screen readers are expected to
#. * present this string alone or as a token in a list.
#.
#: atk/atkvalue.c:222
msgid "very strong"
msgstr "धेरै बलियो"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:230
msgid "very low"
msgstr "अति न्यून"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:238
msgid "medium"
msgstr "मध्यम"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:246
msgid "high"
msgstr "उच्च"

#. Translators: This string describes a range within value-related
#. * widgets such as a volume slider. Note that what such a widget
#. * presents (e.g. temperature, volume, price) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:254
msgid "very high"
msgstr "अति उच्च"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:262
msgid "very bad"
msgstr "धेरै खराब"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:270
msgid "bad"
msgstr "खराब"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:278
msgid "good"
msgstr "असल"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:286
msgid "very good"
msgstr "अति असल"

#. Translators: This string describes a range within value-related
#. * widgets such as a hard drive usage. Note that what such a widget
#. * presents (e.g. hard drive usage, network traffic) is controlled by
#. * application developers. Thus assistive technologies such as screen
#. * readers are expected to present this string alone or as a token in
#. * a list.
#.
#: atk/atkvalue.c:294
msgid "best"
msgstr "उत्तम"

#~ msgid "Is used to notify that the parent has changed"
#~ msgstr "प्रमूल परिवर्तन भइसक्यो भनेर सूचीत गर्न प्रयोग गरिन्छ"
